..
   sphinx-build -b html source build

Welcome to py3DPolyS_LE's documentation!
==============================================================================

.. toctree::
   :maxdepth: 2

   cmd

.. automodule:: py3dpolys_le
    :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
