# Release notes #

## 2024.1

### Added
- Continue option: continue simulation with different configuration scenario

## 2023.5

### Added 
- interaction sites to simulate liquid-liquid phase separation

### Changed
- upgraded HiC3D to handle higher resolutions with unique HiC3D points up to 9223372036854775807= huge(8 bytes integer)
- changed installation to using mamba and 
- upgraded to Python 3.10

## 2022.9

### Initial release as of publication https://doi.org/10.1093/bioinformatics/btac705

