#! /bin/bash
 /builds/togop/3DPolyS-LE/py3dpolys_le/bin/cmd.sh  mpirun 3dpolys_le -o:./out-8860 --km:0.0027 --nlef:200 -b:py3dpolys_le/data/ce/dcc_mex-sites_boundaries.csv  -bd:0 -z  -im:z  -r:2.84  ./test/test_shell_input.cfg
 /builds/togop/3DPolyS-LE/py3dpolys_le/bin/cmd.sh  mpirun 3dpolys_le -o:./out-8860 --km:0.0027 --nlef:200 -b:py3dpolys_le/data/ce/dcc_mex-sites_boundaries.csv  -bd:0 -z  -im:z -a:./out-8860/r2.84 -r:2.84  ./test/test_shell_input.cfg
 /builds/togop/3DPolyS-LE/py3dpolys_le/bin/cmd.sh  3dpolys_le_stats -o ./out-8860 -a ./out-8860/r2.84 --km 0.0027 --nlef 200 -e ./test/data/wt_N2_Moushumi2020_HIC1_5000.cool -b py3dpolys_le/data/ce/dcc_mex-sites_boundaries.csv -bd 0 -r 2.84  -i ./test/test_shell_input.cfg -f ./sim_stats.csv --cmp_chrs chrX X 6
 /builds/togop/3DPolyS-LE/py3dpolys_le/bin/cmd.sh  3dpolys_le_runner multi_decay_plot -o ./out-8860 -i ./test/test_shell_input.cfg -e ./test/data/wt_N2_Moushumi2020_HIC1_5000.cool --cmp_chrs chrX X 6
